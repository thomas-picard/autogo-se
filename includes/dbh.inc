<?php
    class dbh {
        private $handler;

        function __construct() {
            $this->handler = new PDO(
                sprintf(
                    'pgsql:host=%s;port=%s;dbname=%s;user=%s;password=%s',
                    $_ENV{'DB_HOST'},
                    $_ENV{'DB_PORT'},
                    $_ENV{'DB_DATABASE'},
                    $_ENV{'DB_USERNAME'},
                    $_ENV{'DB_PASSWORD'}
                )
            );

            $this->handler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->handler->beginTransaction();
        }



        function count($sql, $values) {
            $statement = $this->handler->prepare($sql);
            $statement->execute($values);
            return $statement->rowCount();
        }

        function fetch($sql, $values) {
            $statement = $this->handler->prepare($sql);
            $statement->execute($values);
            return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }
    }

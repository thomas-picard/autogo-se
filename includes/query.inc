<?php
    class query {
        private $table = 'listings_search';
        private $fields = [];
        private $limit = 24;
        private $offset;
        private $where = [];

        private $dbh;
        private $values;

        function __construct($dbh) {
            $this->dbh = $dbh;
        }



        function count () {
            return $this->dbh->count(
                $this->get_statement([ 'count' => true ]),
                $this->get_values()
            );
        }

        function fetch () {
            return $this->dbh->fetch(
                $this->get_statement(),
                $this->get_values()
            );
        }



        function get_statement ($options = []) {
            $bits = [
                sprintf('SELECT %s', implode(', ', $this->fields) ?: '*'),
                sprintf('FROM %s', $this->table),
            ];

            if ($this->where) {
                $bits[] = sprintf('WHERE %s', implode(' AND ', $this->where));
            }

            if (!$options['count']) {
                if ($this->limit) {
                    $bits[] = sprintf('LIMIT %d', $this->limit);
                }

                if ($this->offset) {
                    $bits[] = sprintf('OFFSET %d', $this->offset);
                }
            }

            return implode($bits, ' ');
        }

        function get_values () {
            return $this->values;
        }



        // getters / setters
        function fields (?array $fields) {
            if (!isset($fields)) {
                return $this->fields;
            }

            $this->fields = array_unique(array_merge($this->fields, $fields));
            return $this;
        }

        function limit (?int $limit) {
            if (!isset($limit)) {
                return $this->limit;
            }

            $this->limit = $limit;
            return $this;
        }

        function offset (?int $offset) {
            if (!isset($offset)) {
                return $this->offset;
            }

            $this->offset = $offset;
            return $this;
        }

        function where (?string $operator, ...$arguments) {
            if (!isset($operator)) {
                return $this->where;
            }

            switch (strtoupper($operator)) {
                case '<':
                case '<=':
                case '=':
                case '>=':
                case '>':
                    $this->where[] = sprintf(
                        '%s %s ?',
                        $arguments[0],
                        $operator,
                    );
                    $this->values[] = $arguments[1];
                    break;
                case 'RADIUS':
                    // arguments[0] is an array [latitude, longitude]
                    // arguments[1] is the radius
                    $this->where[] = 'EARTH_DISTANCE(LL_TO_EARTH(?, ?), LL_TO_EARTH(latitude, longitude))::NUMERIC / 1000 < ?';
                    $this->values[] = $arguments[0][0];
                    $this->values[] = $arguments[0][1];
                    $this->values[] = $arguments[1];
                    break;
            }

            return $this;
        }
    }

<?php
    error_reporting(E_ALL);
    require 'includes/dbh.inc';
    require 'includes/query.inc';

    foreach (parse_ini_file('./.env') as $key => $value) {
        $_ENV[$key] = $value;
    }

    $dbh = new dbh();

    $query = new query($dbh);
    $query->fields([ 'id' ]);

    $coordinates = preg_match('/^(-?\d+(?:\.\d+)?),(-?\d+(?:\.\d+)?)/', $_GET['latlng'], $matches)
        ? [ (float)$matches[1], (float)$matches[2] ]
        : [ 45.505331312, -73.55249779 ];

    if ($_GET['brand_id']) {
        $query->where('=', 'brand_id', (int)$_GET['brand_id']);
    }

    if ($_GET['radius']) {
        $query->where('RADIUS', $coordinates, (int)$_GET['radius']);
    }

    var_dump([
        $query->get_statement(),
        $query->get_values()
    ]);

    var_dump([
        'count' => $query->count(),
        'listing_ids' => array_map(
            fn ($record) => $record['id'],
            $query->fetch()
        ),
    ]);
